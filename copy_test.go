package fastcopy_test

import (
	"bytes"
	"errors"
	"io"
	"testing"

	"codeberg.org/gruf/go-fastcopy"
)

// A version of bytes.Buffer without ReadFrom and WriteTo
type Buffer struct {
	bytes.Buffer
	io.ReaderFrom // conflicts with and hides bytes.Buffer's ReaderFrom.
	io.WriterTo   // conflicts with and hides bytes.Buffer's WriterTo.

}

// Simple tests, primarily to verify the ReadFrom and WriteTo callouts inside fastcopy.Copy.

func TestCopy(t *testing.T) {
	rb := new(Buffer)

	wb := new(Buffer)

	rb.WriteString("hello, world.")

	fastcopy.Copy(wb, rb)

	if wb.String() != "hello, world." {
		t.Errorf("Copy did not work properly")
	}
}

func TestCopyNegative(t *testing.T) {
	rb := new(Buffer)

	wb := new(Buffer)

	rb.WriteString("hello")

	fastcopy.Copy(wb, &io.LimitedReader{R: rb, N: -1})

	if wb.String() != "" {
		t.Errorf("Copy on LimitedReader with N<0 copied data")
	}

	fastcopy.CopyN(wb, rb, -1)

	if wb.String() != "" {
		t.Errorf("CopyN with N<0 copied data")
	}
}

func TestCopyReadFrom(t *testing.T) {
	rb := new(Buffer)

	wb := new(bytes.Buffer) // implements ReadFrom.

	rb.WriteString("hello, world.")

	fastcopy.Copy(wb, rb)

	if wb.String() != "hello, world." {
		t.Errorf("Copy did not work properly")
	}
}

func TestCopyWriteTo(t *testing.T) {
	rb := new(bytes.Buffer) // implements WriteTo.

	wb := new(Buffer)

	rb.WriteString("hello, world.")

	fastcopy.Copy(wb, rb)

	if wb.String() != "hello, world." {
		t.Errorf("Copy did not work properly")
	}
}

// Version of bytes.Buffer that checks whether WriteTo was called or not
type writeToChecker struct {
	bytes.Buffer
	writeToCalled bool
}

func (wt *writeToChecker) WriteTo(w io.Writer) (int64, error) {
	wt.writeToCalled = true
	return wt.Buffer.WriteTo(w)
}

// It's preferable to choose WriterTo over ReaderFrom, since a WriterTo can issue one large write,
// while the ReaderFrom must read until EOF, potentially allocating when running out of buffer.
// Make sure that we choose WriterTo when both are implemented.

func TestCopyPriority(t *testing.T) {
	rb := new(writeToChecker)

	wb := new(bytes.Buffer)

	rb.WriteString("hello, world.")

	fastcopy.Copy(wb, rb)

	if wb.String() != "hello, world." {
		t.Errorf("Copy did not work properly")
	} else if !rb.writeToCalled {
		t.Errorf("WriteTo was not prioritized over ReadFrom")
	}
}

type zeroErrReader struct {
	err error
}

func (r zeroErrReader) Read(p []byte) (int, error) {
	return copy(p, []byte{0}), r.err
}

type errWriter struct {
	err error
}

func (w errWriter) Write([]byte) (int, error) {
	return 0, w.err
}

// In case a Read results in an error with non-zero bytes read, and
// the subsequent Write also results in an error, the error from Write
// is returned, as it is the one that prevented progressing further.

func TestCopyReadErrWriteErr(t *testing.T) {
	er, ew := errors.New("readError"), errors.New("writeError")

	r, w := zeroErrReader{err: er}, errWriter{err: ew}

	n, err := fastcopy.Copy(w, r)

	if n != 0 || err != ew {
		t.Errorf("Copy(zeroErrReader, errWriter) = %d, %v; want 0, writeError", n, err)
	}
}
